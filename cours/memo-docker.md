# Memo Docker 

<!-- vim-markdown-toc GitLab -->

* [Concept](#concept)
* [Commandes](#commandes)
    * [Dockerfile](#dockerfile)
    * [Build](#build)
    * [Récupérer une image externe](#récupérer-une-image-externe)

<!-- vim-markdown-toc -->

# Concept

Docker est un outil de conteneurisation. Cross-platform, vous pourrez l'installer et le faire fonctionner nativement sous GNU/Linux, MacOS ou Windows.

Un "conteneur" c'est un environnement isolé qui vit au sein de votre système. C'est semblable à une VM (avec la virtualisation du hardware (CPU, RAM, dique, etc.) en moins).

Un conteneur est **obligé** d'exécuter un processus, une commande. S'il n'exécute rien, il quitte (dans le cas où on ne lance aucune commande ou si la commande qu'on a lancé dans le conteneur s'est terminée).

---

Pour créer un conteneur, on instancie ce qu'on appelle une "image". Une image, quant à elle, est construire à partir de la rédaction d'un Dockerfile.

Le workflow classique :
1. Création d'un Dockerfile
2. "Build" du Dockerfile, afin de créer une image
3. Instanciation de l'image pour créer un conteneur

> Attention, une image Docker est complètement statique, il est impossible de la modifier une fois créée. Pour mettre à jour une image donnée, on en reconstruit une nouvelle.

# Commandes

Lancer un conteneur
```bash
$ docker run <OPTIONS> <IMAGE> <COMMANDE>

# Lancement d'un conteneur simple, basé sur Debian, qui quitte tout de suite
$ docker run debian

# Lancement d'un conteneur simple, basé sur Debian, qui exécute un sleep en tâche de fond
# Si ce sleep se termine, le conteneur quitte
$ docker run -d debian sleep 9999

# Lancement d'un conteneur simple, basé sur Debian, qui exécute un terminal
# Si vous quittez le terminal, le conteneur quitte
$ docker run -it debian bash
```

Agir sur les conteneurs existants :
```bash
# Lister les conteneurs actifs
$ docker ps

# Lister les conteneurs actifs et inactifs
$ docker ps -a

# Supprimer un conteneur
$ docker rm -f <NAME_OR_ID>
```

Récupérer un terminal dans un conteneur existant :
```bash
$ docker exec -it <NAME_OR_ID> <SHELL>

# Par exemple
$ docker exec -it <NAME_OR_ID> bash
$ docker exec -it <NAME_OR_ID> sh
```

## Dockerfile

Exemple de Dockerfile :

```Dockerfile
FROM debian:jessie

RUN apt-get update -y

RUN apt-get install -y vim


COPY test /opt/test

# Définit un répertoire de travail depuis lesquelles toutes les commandes suivantes seront exécutées
# Y compris le ENTRYPOINT, pratique pour lancer une commande depuis la racine de votre projet
WORKDIR /opt

ENTRYPOINT /bin/sleep 9999
```

## Build

Pour build un Dockerfile en une image :
```bash
$ ls 
Dockerfile

$ docker build -t <NAME> <CONTEXT>

# Par exemple, si le Dockerfile est dans le répertoire courant
$ docker build -t my_image .
```

## Récupérer une image externe

On peut récupérer, depuis la ligne de commande, des images hébergées sur des registres Docker externes. Par défaut, la commande `docker` va chercher les images qu'on lui demande sur le [Docker Hub](https://hub.docker.com) mais il est possible de faire appel à un registre spécifique.

Récupération d'images externes :
```bash
# Récupère l'image debian sur le Docker Hub
$ docker pull debian

# Récupère une image debian en version Jessie sur le Docker Hub
$ docker pull debian:jessie

# Récupérer une image sur un registre privée (comme le registre intégré à Gitla)
# Pour Gitlab spécifiquement, vous trouverez le nom complet de l'image sur la page "Container Registry" de votre dépôt
$ docker login registry.gitlab.com
$ docker pull registry.gitlab.com/it4lik/<IMAGE_NAME>
```
